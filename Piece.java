public class Piece {
    char type;
    boolean white; 
    int posX;
    int posY;
    boolean passant;
    boolean moved;

    public Piece(int x, int y, char t, boolean isWhite) {
        char[] pieces = {'p', 'r', 'b', 'k', 'K', 'Q'};
        boolean create = false;
        for (char p : pieces) {
            if (t == p) {
                create = true;
            }
        }
        if (create) {
            type = t;
            white = isWhite;
            posX = x;
            posY = y;
            passant = false;
            moved = false;
        }
    }

    public boolean getPassant() {
        return passant;
    }

    public void setPassant(boolean p) {
        passant = p;
    }

    public char type() {
        return type;
    }

    public boolean isWhite() {
        return white;
    }

    public boolean isBlack() {
        return !white;
    }

    public int posX () {
        return posX;
    }

    public int posY () {
        return posY;
    }

    public void setPos(int x, int y) {
        posX = x;
        posY = y;
    }

    public String symbol() {
        if (white) {
            if (type == 'p') {
                return "\u2659";
            } else if (type == 'r') {
                return "\u2656";
            } else if (type == 'k') {
                return "\u2658";
            } else if (type == 'b') {
                return "\u2657";
            } else if (type == 'K') {
                return "\u2654";
            } else if (type == 'Q') {
                return "\u2655";
            }
        } else {
            if (type == 'p') {
                return "\u265F";
            } else if (type == 'r') {
                return "\u265C";
            } else if (type == 'k') {
                return "\u265E";
            } else if (type == 'b') {
                return "\u265D";
            } else if (type == 'K') {
                return "\u265A";
            } else if (type == 'Q') {
                return "\u265B";
            }
        }
        return " ";

    }
}
