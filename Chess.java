import java.lang.Math;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Chess {

    public static int linesPrinted = 0;
    public static boolean gameOver = false;

    public static ArrayList<String> whiteMoves = new ArrayList<String>();
    public static ArrayList<String> blackMoves = new ArrayList<String>();
    
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_BRIGHTWHITE = "\u001B[97m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_BRIGHTBLACK_BACKGROUND = "\u001B[100m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static Piece board[][] =  new Piece[8][8];
    public static boolean whiteTurn = true;
    public static boolean neatPrinting = false;

    public static boolean custom = true;



    public static String whitePieces[] = {"........", 
                                          "........", 
                                          "........", 
                                          "........",
                                          "........", 
                                          "........",
                                          "pppppppp", 
                                          "rkbQKbkr"};

    public static String blackPieces[] = {"rkbQKbkr", 
                                          "pppppppp", 
                                          "........", 
                                          "........",
                                          "........", 
                                          "........",
                                          "........", 
                                          "........"};

    
    public static String customWhite[] = {"........", 
                                          ".......p", 
                                          "........", 
                                          "........",
                                          "........", 
                                          "........",
                                          "ppppppp.", 
                                          "r...K.kr"};

    public static String customBlack[] = {"rkbQKbk.", 
                                          "ppppppp.", 
                                          "........", 
                                          ".....Q..",
                                          ".......p", 
                                          "........",
                                          ".......p", 
                                          "........"};



    public static void main(String[] args) {

        File game;

        if (args.length == 1) {
            game = new File(args[0]);
            if (game != null) {
                playback(game);
            }
        } else {
            Scanner s = new Scanner(System.in);
            setBoard();
            printBoard();

            while(!gameOver) {
                System.out.print("Enter move: ");
                String move = s.nextLine();
                linesPrinted += 1;
                stringToMove(move);
                printBoard();
                System.out.println("gameover:" + gameOver);
            }
        }
    }

    public static void playback(File game) {

        try {


            Scanner gameReader = new Scanner(game);
            int tokenCycle = 0;

            while(gameReader.hasNext()) {
                String token = gameReader.next();
                if (token.charAt(0) == '[') {
                    boolean skip = true;
                    while (skip) {
                        token = gameReader.next();
                        if (token.charAt(token.length()-1) == ']') {
                            skip = false;
                        }
                    }
                } else if (token.charAt(0) == '{') {
                    boolean skip = true;
                    while (skip) {
                        token = gameReader.next();
                        if (token.charAt(token.length()-1) == '}') {
                            skip = false;
                        }
                    }
                } else {
                    if (tokenCycle == 0) {
                        tokenCycle ++;
                    } else if (tokenCycle == 1) {
                        whiteMoves.add(token);
                        tokenCycle ++;
                    } else if (tokenCycle == 2) {
                        blackMoves.add(token);
                        tokenCycle = 0;
                        
                    }
                    // System.out.print(token + " ");
                }
                if (token.charAt(token.length()-1) == '#') {
                    break;
                }
                           
            }


        } catch (IOException e) {
            System.out.println("error");
        }

        for (int i = 0; i < whiteMoves.size(); i ++) {
            System.out.print(whiteMoves.get(i));
            if (i < blackMoves.size()) {
                for (int j = 0; j < 10-whiteMoves.get(i).length(); j ++) {
                    System.out.print(" ");
                }
                System.out.println(blackMoves.get(i));
            }
        }

        System.out.println();

    }

    public static void stringToMove(String move) {
        if (move.matches("\\w{1}\\d{1} \\w{1}\\d{1}")) {
            int x1 = (int)move.charAt(0)-97;
            int y1 = (int)move.charAt(1)-49;
            int x2 = (int)move.charAt(3)-97;
            int y2 = (int)move.charAt(4)-49;
            move(board[y1][x1], x2, y2);
        }
    }

    public static void pause() {
        try
        {
            Thread.sleep(1000);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }

    public static void move(Piece piece, int x, int y) {
        if (piece != null) {
            if (piece.isWhite() == whiteTurn) {
                Piece oldPiece = board[y][x];
                int oldX = piece.posX();
                int oldY = piece.posY();

                if (isLegal(piece, x, y)) { 
                    if (piece.type() == 'p') {
                        if (board[piece.posY()][x]!=null) {
                            if (piece.isWhite() != board[piece.posY()][x].isWhite() && board[piece.posY()][x].getPassant() == true && (x == piece.posX() + 1 || x == piece.posX() - 1)) {
                                board[piece.posY()][x] = null;
                            }
                        }
                    }
                    if (piece.type() == 'p' && Math.abs(piece.posY() - y) == 2) {
                        for (int j = 0; j < 8; j ++) {
                            for (int i = 0; i < 8; i ++) {
                                if (board[j][i]!= null) {
                                    board[j][i].setPassant(false);
                                }
                            }
                        }
                        piece.setPassant(true);
                    } else {
                        for (int j = 0; j < 8; j ++) {
                            for (int i = 0; i < 8; i ++) {
                                if (board[j][i]!= null) {
                                    board[j][i].setPassant(false);
                                }
                            }
                        }
                    }

                    if (piece.type == 'p') {
                        if ((piece.isWhite() && y == 7) || (!piece.isWhite() && y == 0)) {
                            boolean cont = false;
                            while (!cont) {
                                Scanner s = new Scanner(System.in);
                                System.out.print("Select promotion: ");
                                linesPrinted ++;
                                String promotion = s.nextLine();
                                if (promotion.charAt(0) == 'Q' || promotion.charAt(0) == 'k' || promotion.charAt(0) == 'r') {
                                    piece.type = promotion.charAt(0);
                                    cont = true;
                                }
                            }
                        }
                    }

                    if (piece.type() == 'K' && Math.abs(piece.posX - x) == 2) {
                        if (x == 6) {
                            Piece rook = board[y][7];
                            board[y][7] = null;
                            rook.setPos(5, y);
                            board[y][5] = rook;
                        } else {
                            Piece rook = board[y][0];
                            board[y][0] = null;
                            rook.setPos(3, y);
                            board[y][3] = rook;
                        }
                    }

                    board[piece.posY()][piece.posX()] = null;
                    piece.setPos(x, y);
                    board[y][x] = piece;
                    if (inCheck(whiteTurn)) {
                        piece.setPos(oldX, oldY);
                        board[oldY][oldX] = piece;
                        board[y][x] = oldPiece;
                        piece.setPassant(false);
                    } else {
                        whiteTurn = !whiteTurn;
                        piece.moved = true;
                    }
                    
                }

            }
        }

        

    }

    public static boolean attacks(boolean white, int x, int y) {

        for (int j = 0; j < 8; j ++) {
            for (int i = 0; i < 8; i ++) {
                if (!(j != y && i != x) && board[j][i] != null && board[j][i].isWhite() != white && isLegal(board[j][i], x, y)) {
                    System.out.println("here");
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean inCheck(boolean white) {
        int x = -1;
        int y = -1;
        for (int j = 0; j < 8; j ++) {
            for (int i = 0; i < 8; i ++) {
                if (board[j][i]!= null && board[j][i].isWhite() == white && board[j][i].type() == 'K') {
                    x = i;
                    y = j;
                }
            }
        }

        for (int j = 0; j < 8; j ++) {
            for (int i = 0; i < 8; i ++) {
                if (!(j != y && i != x) && board[j][i] != null && board[j][i].isWhite() != white && isLegal(board[j][i], x, y)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkMate(boolean white) {
        boolean ret = true;
        for (int j = 0; j < 8 && ret == true; j ++) {
            for (int i = 0; i < 8 && ret == true; i ++) {
                for (int y = 0; y < 8 && ret == true; y ++) {
                    for (int x = 0; x < 8 && ret == true; x ++) {
                        Piece piece = board[j][i];
                        if (piece != null) {
                            if (piece.isWhite() == whiteTurn) {
                                Piece oldPiece = board[y][x];
                                int oldX = piece.posX();
                                int oldY = piece.posY();
                                if (isLegal(piece, x, y)) {
                                    board[piece.posY()][piece.posX()] = null;
                                    piece.setPos(x, y);
                                    board[y][x] = piece;
                                    if (inCheck(whiteTurn)) {
                                        piece.setPos(oldX, oldY);
                                        board[oldY][oldX] = piece;
                                        board[y][x] = oldPiece;
                                    } else {

                                        piece.setPos(oldX, oldY);
                                        board[oldY][oldX] = piece;
                                        board[y][x] = oldPiece;
                                        ret = false;
                                        
                                        
                                    }
                                    
                                }

                            }
                        }
                    }
                }
            }
        }

        return ret;
    }

    public static boolean isLegal(Piece piece, int x, int y) {
        if (x < 0 || x > 7 || y < 0 || y > 7) {
            return false;
        }
        if (piece == null) {
            return false;
        }
        if (piece.type() == 'p') {
            if (piece.posX() == x && piece.posY() == y) {
                return false;
            }
            if (piece.isWhite()) {
                if (board[piece.posY()][x]!=null) {
                    if (piece.isWhite() != board[piece.posY()][x].isWhite() && board[piece.posY()][x].getPassant() && (x == piece.posX() + 1 || x == piece.posX() - 1)) {
                        return true;
                    }
                }
                if (piece.posX() == x && piece.posY() == 1 && y == 3) {
                    if (isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                        return true;
                    } 
                }
                if (y == piece.posY() + 1 && x == piece.posX() && board[y][x] == null) {
                    return true;
                }

                if (y == piece.posY() + 1 && (x == piece.posX() + 1 || x == piece.posX() - 1) && board[y][x] != null) {
                    if (board[y][x].isBlack()){
                        return true;
                    }
                }

            } else {
                if (board[piece.posY()][x]!=null) {
                    if (piece.isWhite() != board[piece.posY()][x].isWhite() && board[piece.posY()][x].getPassant() == true && (x == piece.posX() + 1 || x == piece.posX() - 1)) {
                        return true;
                    }
                }
                if (piece.posX() == x && piece.posY() == 6 && y == 4) {
                    if (isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                        return true;
                    } 
                }

                if (y == piece.posY() - 1 && x == piece.posX() && board[y][x] == null) {
                    return true;
                }

                if (y == piece.posY() - 1 && (x == piece.posX() - 1 || x == piece.posX() - 1) && board[y][x] != null) {
                    if (board[y][x].isWhite()){
                        return true;
                    }
                }
            }

            return false;

        } else if (piece.type() == 'r') {
            if (piece.posX() == x && piece.posY() == y) {
                return false;
            }
            if ((y == piece.posY() && x != piece.posX()) || (y != piece.posY() && x == piece.posX())) {
                if (isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                    if (board[y][x] != null) {
                        if (board[y][x].isWhite() == piece.isWhite()) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else if (piece.type() == 'k') {
            if (piece.posX() == x && piece.posY() == y) {
                return false;
            }
            if ((Math.abs(piece.posX()-x) == 2 && Math.abs(piece.posY()-y) == 1) || (Math.abs(piece.posX()-x) == 1 && Math.abs(piece.posY()-y) == 2)) {
                if (board[y][x] != null) {
                    if (board[y][x].isWhite() == piece.isWhite()) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        } else if (piece.type() == 'b') {
            if (piece.posX() == x && piece.posY() == y) {
                return false;
            }

            if (Math.abs(piece.posX()-x) == Math.abs(piece.posY()-y)) {
                
                if (isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                    if (board[y][x] == null) {
                        return true;
                    } else {
                        if (board[y][x].isWhite() == piece.isWhite()) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if (piece.type() == 'Q') {
            if (piece.posX() == x && piece.posY() == y) {
                return false;
            }
            if (isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                if (board[y][x] != null) {
                    if (board[y][x].isWhite() == piece.isWhite()) {
                        return false;
                    } else {
                        // System.out.println(piece.posX() + " " + piece.posY() + " " + x + " " + y);
                        return true;
                    }
                } else {
                    return true;
                }
            }
        } else if (piece.type() == 'K') {
            if (Math.abs(piece.posX() - x) < 2 && Math.abs(piece.posY() - y) < 2) {
                if (board[y][x] != null) {
                    if (board[y][x].isWhite() != piece.isWhite()) {
                        return true;
                    } else  {
                        return false;
                    }
                } else {
                    return true;
                }
            } else if (y == piece.posY() && x == piece.posX + 2) {
                if (inCheck(whiteTurn)) {
                    return false;
                } else {
                    if (attacks(whiteTurn, piece.posX()+1, piece.posY()) || attacks(whiteTurn, piece.posX()+2, piece.posY())) {
                        return false;
                    }
                    if (piece.moved) {
                        return false;
                    }
                    if (board[piece.posY()][7] == null || board[piece.posY()][7].moved) {
                        return false;
                    }
                    if (!isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                        return false;
                    }

                    return true;

                }
            } else if (y == piece.posY() && x == piece.posX - 2) {
                if (inCheck(whiteTurn)) {
                    return false;
                } else {
                    if (attacks(whiteTurn, piece.posX()-1, piece.posY()) || attacks(whiteTurn, piece.posX()-2, piece.posY())) {
                        return false;
                    }
                    if (piece.moved) {
                        return false;
                    }
                    if (board[piece.posY()][0] == null || board[piece.posY()][0].moved) {
                        return false;
                    }
                    if (!isClearAndStraight(piece.posX(), x, piece.posY(), y)) {
                        return false;
                    }

                    return true;
                }
            }
            
            {
                return false;
            }
        }

        return false;


    }

    public static boolean isClearAndStraight(int x1, int x2, int y1, int y2) {
        if (!(x1 == x2 || y1 == y2 || (Math.abs(x1-x2) == Math.abs(y1-y2)))) {
            return false;
        }

        if (Math.abs(x1-x2) == Math.abs(y1-y2)) {
            int a = 1;
            int b = 1;

            if (x1 > x2) {
                a = -1;
            }

            if (y1 > y2) {
                b = -1;
            }

            for (int i = 1; i < Math.abs(x1-x2); i++) {
                if (board[y1+(b*i)][x1+(a*i)] != null) {
                    return false;
                    
                }
            }

        } else if (x1 < x2) {
            for (int i = x1+1; i < x2; i ++) {
                if (board[y2][i] != null) {
                    return false;
                }
            }
        } else if (x1 > x2) {
            for (int i = x2+1; i < x1; i ++) {
                if (board[y2][i] != null) {
                    return false;
                }
            }
        } else if (y1 < y2) {
            for (int i = y1+1; i < y2; i ++) {
                if (board[i][x2] != null) {

                    return false;
                }
            }
        } else if (y1 > y2) {
            for (int i = y2+1; i < y1; i ++) {
                if (board[i][x2] != null) {
                    return false;
                }
            }
        }

        return true;

    }

    public static void setBoard() {
        String white[] = whitePieces;
        String black[] = blackPieces;
        if (custom) {
            white = customWhite;
            black = customBlack;
        }

        for (int y = 0; y < 8; y ++) {
            for (int x = 0; x < 8; x ++) {
                char[] pieces = {'p', 'r', 'b', 'k', 'K', 'Q'};
                boolean createWhite = false;
                boolean createBlack = false;
                for (char p : pieces) {
                    if (white[7-y].charAt(x) == p) {
                        createWhite = true;
                    }
                    if (black[7-y].charAt(x) == p) {
                        createBlack = true;
                    }
                }

                if (createWhite) {
                    Piece p = new Piece(x, y, white[7-y].charAt(x), true);
                    board[y][x] = p;
                }
                if (createBlack) {
                    Piece p = new Piece(x, y, black[7-y].charAt(x), false);
                    board[y][x] = p;
                }
            }

        }

    }

    public static void printBoard() {
        if (neatPrinting) {
            for (int i = 0; i < linesPrinted; i ++) {
                System.out.printf("\u001B[1A");
                System.out.printf("\u001B[2K");
            }
            linesPrinted = 0;
        }
        
        char[] alp = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

        System.out.println();
        System.out.println();
        linesPrinted += 2;
        for (int j = 7; j >=0; j --) {
            System.out.print(j+1 + "  ");
            for (int i = 0; i < 8; i ++) {
                if ((i+j)%2 == 0) {
                    System.out.print(ANSI_BRIGHTBLACK_BACKGROUND);
                } else {
                    System.out.print(ANSI_WHITE_BACKGROUND);
                }
                if (board[j][i] != null) {
                    if (board[j][i].isWhite()) {
                        System.out.print(ANSI_BRIGHTWHITE);
                    } else {
                        System.out.print(ANSI_BLACK);
                    }
                    System.out.print(' ');
                    System.out.print(board[j][i].type());
                    System.out.print(' ');
                } else {
                    System.out.print("   ");
                }
                System.out.print(ANSI_RESET);
            }
            System.out.println();
            linesPrinted += 1;

        }
        System.out.print("   ");

        for (int i = 0; i < 8; i ++) {
            
            System.out.print(" " + alp[i] + " ");

        }
        System.out.println();
        linesPrinted += 1;

        if (inCheck(whiteTurn)) {
            if (checkMate(whiteTurn)) {
                System.out.println("check mate!");
                gameOver = true;
                linesPrinted += 1;
            } else {
                System.out.println("check!");
                linesPrinted += 1;
            }
        }
    }

    
}